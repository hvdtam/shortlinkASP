using System.ComponentModel.DataAnnotations;

namespace shortLink.Models.Auth;

public class SignUpModel
{
    [Required, EmailAddress]
    public string Email {get; set;} = null!;
    [Required]
    public string Password {get; set;} = null!;
    [Required]
    private string ConfirmPassword {get; set;} = null!;
}
