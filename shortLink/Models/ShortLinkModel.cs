using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using shortLink.Data;
using shortLink.Helpers;

namespace shortLink.Models;
[Table("ShortLinks")]
public class ShortLinkModel
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required]
    [StringLength(1000, MinimumLength = 5)]
    public string? LongUrl { get; set; }

    // [Required]

    [StringLength(100, MinimumLength = 3)]
    [Index("Idx_AliasUrl",IsUnique = true)]
    public string AliasUrl { get; set; } = null!;

    // [Required]
    [StringLength(200, MinimumLength = 5)]
    [Index("Idx_FullAliasUrl", Order = 1, IsUnique = true)]
    public string FullAliasUrl { get; set; } = RandomString.GenerateRandomString(10);

    [JsonIgnore]
    public string? Password { get; set; }

    [DefaultValue(10)] public int Status { get; set; }

    public DateTimeOffset? Expire { get; set; }

    [DefaultValue(0)] public int? TotalClick { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    [JsonIgnore]
    public int? CreatedBy { get; set; }

    [JsonIgnore]
    public DateTimeOffset UpdatedAt { get; set; }

    [JsonIgnore]
    public int? UpdatedBy { get; set; }
}
