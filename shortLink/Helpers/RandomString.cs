using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using shortLink.Data;

namespace shortLink.Helpers;

public class RandomString
{
    public static string GenerateRandomString(int length)
    {
        const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        var res = new StringBuilder();
        using var rng = new RNGCryptoServiceProvider();
        var uintBuffer = new byte[sizeof(uint)];

        while (length-- > 0)
        {
            rng.GetBytes(uintBuffer);
            var num = BitConverter.ToUInt32(uintBuffer, 0);
            res.Append(valid[(int)(num % (uint)valid.Length)]);
        }

        // var isAliasUrlExist = CheckIfAliasUrlExist(res.ToString(), context);

        // if (isAliasUrlExist)
        // {
            // return GenerateRandomString(length);
        // }

        return res.ToString();
    }

    // private static bool CheckIfAliasUrlExist(string aliasUrl, ShortLinkContext context)
    // {
        // var shortLink = context.ShortLinks!.FirstOrDefault(x => x.AliasUrl == aliasUrl);
        // return shortLink != null;
    // }
}
