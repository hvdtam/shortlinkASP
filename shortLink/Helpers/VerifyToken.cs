using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using shortLink.Data;

namespace shortLink.Helpers
{
    public class VerifyToken
    {
        private readonly IConfiguration _configuration;

        public VerifyToken(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GetEmailFromToken(string tokenString)
        {

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["JWT:Secret"]);


            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            };

            SecurityToken validatedToken;
            var claimsPrincipal = tokenHandler.ValidateToken(tokenString, validationParameters, out validatedToken);


            var emailClaim = claimsPrincipal.FindFirst(ClaimTypes.Email);
            return emailClaim!.Value;
        }
    }
}
