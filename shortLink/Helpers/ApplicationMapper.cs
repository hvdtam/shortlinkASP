using AutoMapper;
using shortLink.Data;
using shortLink.Models;


namespace shortLink.Helpers;

public class ApplicationMapper : Profile
{
    public ApplicationMapper()
    {
        CreateMap<ShortLink, ShortLinkModel>().ReverseMap();
    }
}
