using Microsoft.AspNetCore.Cryptography.KeyDerivation;
namespace shortLink.Helpers;
public class HashedPassword
{
    public static string HashPassword(string password)
    {
        var salt = new byte[10];

        byte[] hash = KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 10000,
            numBytesRequested: 256 / 8);

        return Convert.ToBase64String(hash);
    }

    public static bool VerifyPassword(string password, string hashedPassword)
    {
        var salt = new byte[10];
        byte[] hash = Convert.FromBase64String(hashedPassword);

        byte[] newHash = KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 10000,
            numBytesRequested: 256 / 8);

        return ByteArraysEqual(hash, newHash);
    }

    private static bool ByteArraysEqual(byte[] a, byte[] b)
    {
        if (a == null && b == null)
        {
            return true;
        }
        if (a == null || b == null || a.Length != b.Length)
        {
            return false;
        }
        else
        {
            bool areEqual = true;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    areEqual = false;
                }
            }
            return areEqual;
        }
    }
}
