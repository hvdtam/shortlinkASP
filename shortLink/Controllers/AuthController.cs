using Microsoft.AspNetCore.Mvc;
using shortLink.Models.Auth;
using shortLink.Repositories;

namespace shortLink.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly IAuthRepository _accountRepository;

    public AuthController(IAuthRepository repository)
    {
        _accountRepository = repository;
    }

    [HttpPost("SignUp")]
    public async Task<IActionResult> SignUp(SignUpModel model)
    {
        var result = await _accountRepository.SignUpAsync(model);
        if (result.Errors.Any())
        {
            var errorResponse = new Dictionary<string, object>
            {
                { "error", result.Errors }
            };
            return BadRequest(errorResponse);
        }

        return Ok(result);
    }

    [HttpPost("SignIn")]
    public async Task<IActionResult> SignIn(SignInModel signInModel)
    {
        var result = await _accountRepository.SignInAsync(signInModel);

        if (string.IsNullOrEmpty(result))
        {
            return Unauthorized();
        }

        return Ok(result);
    }
}
