using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using shortLink.Data;
using shortLink.Helpers;
using shortLink.Repositories;

namespace shortLink.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ShortLinkController : ControllerBase
{
    private readonly ShortLinkContext _context;
    private readonly IShortLinkRepository _shortLinkRepository;
    private readonly IConfiguration _configuration;
    private readonly UserManager<ApplicationUser> _userManager;

    public ShortLinkController(ShortLinkContext context,
        UserManager<ApplicationUser> userManager,
        IConfiguration configuration,
        IShortLinkRepository shortLinkRepository)
    {
        _context = context;
        _configuration = configuration;
        _userManager = userManager;
        _shortLinkRepository = shortLinkRepository;
    }

    // GET: api/ShortLink
    [HttpGet]
    public async Task<IActionResult> GetShortLinks()
    {
        if (_context.ShortLinks == null) return NoContent();
        var verifyToken = new VerifyToken(_configuration);
        var tokenAuth = Request.Headers["Authorization"];
        if (!string.IsNullOrEmpty(tokenAuth))
        {
            var emailFromToken = verifyToken.GetEmailFromToken(tokenAuth.ToString().Split(" ")[1]);
            var user = await _userManager.FindByEmailAsync(emailFromToken);
            if (user != null)
            {
                return Ok(await _shortLinkRepository.GetAllShortLinksAsync(user.Id));
            }

            return NoContent();
        }

        return NoContent();
    }

    // GET: api/ShortLink/abc
    [HttpGet("{aliasUrl}")]
    public async Task<IActionResult> GetShortLink(string aliasUrl)
    {
        var shortLink = await _shortLinkRepository.GetShortLinkByAliasUrlAsync(aliasUrl);
        if (shortLink == null) return NotFound();
        if (shortLink.IsExpired()) return BadRequest("The ShortLink has expired.");
        if (shortLink.IsInActive()) return BadRequest("The ShortLink is not active.");
        if (shortLink.IsPasswordProtected())
        {
            var password = Request.Headers["Password"].ToString();

            if (string.IsNullOrEmpty(password))
            {
                return Unauthorized("Please provide a password to access this ShortLink.");
            }

            var isCorrectPassword = HashedPassword.VerifyPassword(password, shortLink.Password);

            if (isCorrectPassword)
            {
                await _shortLinkRepository.IncreaseTotalClickAsync(aliasUrl);
                return Ok(shortLink);
            }
        }

        await _shortLinkRepository.IncreaseTotalClickAsync(aliasUrl);
        return Ok(shortLink);
    }

    // PUT: api/ShortLink/5
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPut("{id}")]
    [Authorize]
    public async Task<IActionResult> PutShortLink(int id, ShortLink shortLink)
    {
        if (id != shortLink.Id) return BadRequest();
        var verifyToken = new VerifyToken(_configuration);
        var tokenAuth = Request.Headers["Authorization"];
        var existingShortLink = await _shortLinkRepository.GetShortLinkByAliasUrlAsync(shortLink.AliasUrl);
        if (existingShortLink != null)
        {
            return BadRequest("The AliasUrl already exists. Please use a different value for AliasUrl.");
        }
        if (string.IsNullOrEmpty(tokenAuth))
        {
            return Forbid();
        }
        if (!string.IsNullOrEmpty(tokenAuth))
        {
            var emailFromToken = verifyToken.GetEmailFromToken(tokenAuth.ToString().Split(" ")[1]);
            var user = await _userManager.FindByEmailAsync(emailFromToken);
            shortLink.UpdatedBy = user.Id != null ? user.Id : null;
        }

        await _shortLinkRepository.UpdateShortLinkAsync(id,shortLink);
        return Ok();
    }

    // POST: api/ShortLink
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPost]
    public async Task<IActionResult> PostShortLink(ShortLink model)
    {
        try
        {
            var verifyToken = new VerifyToken(_configuration);
            var tokenAuth = Request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(tokenAuth))
            {
                var emailFromToken = verifyToken.GetEmailFromToken(tokenAuth.ToString().Split(" ")[1]);
                var user = await _userManager.FindByEmailAsync(emailFromToken);
                model.CreatedBy = user.Id != null ? user.Id : null;
                model.UpdatedBy = user.Id != null ? user.Id : null;
            }

            var existingShortLink = await _shortLinkRepository.GetShortLinkByAliasUrlAsync(model.AliasUrl);
            if (existingShortLink != null)
            {
                return BadRequest("The AliasUrl already exists. Please use a different value for AliasUrl.");
            }

            var shortLinkId = await _shortLinkRepository.AddShortLinkAsync(model);
            var shortLink = await _shortLinkRepository.GetShortLinkByIdAsync(shortLinkId);

            return shortLink == null ? NotFound() : Ok(shortLink);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return BadRequest();
        }
    }

    // DELETE: api/ShortLink/5
    [HttpDelete("{id}")]
    [Authorize]
    public async Task<IActionResult> DeleteShortLink(int id)
    {
        if (_context.ShortLinks == null) return NotFound();
        var shortLink = await _context.ShortLinks.FindAsync(id);
        var verifyToken = new VerifyToken(_configuration);
        var tokenAuth = Request.Headers["Authorization"];
        if (!string.IsNullOrEmpty(tokenAuth))
        {
            var emailFromToken = verifyToken.GetEmailFromToken(tokenAuth.ToString().Split(" ")[1]);
            var user = await _userManager.FindByEmailAsync(emailFromToken);
            if (user.Id != shortLink!.CreatedBy)
            {
                return Forbid();
            }
        }

        if (string.IsNullOrEmpty(tokenAuth))
        {
            return Forbid();
        }

        if (shortLink == null) return NotFound();

        _context.ShortLinks.Remove(shortLink);
        await _context.SaveChangesAsync();

        return Ok("ShortLink deleted successfully.");
    }
}
