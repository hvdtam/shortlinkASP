using System.Configuration;
using AutoMapper;
using System.Timers;
using Microsoft.EntityFrameworkCore;
using shortLink.Data;
using shortLink.Helpers;
using Timer = System.Timers.Timer;

namespace shortLink.Repositories;

public class ShortLinkRepository : IShortLinkRepository
{
    private readonly ShortLinkContext _context;
    private readonly IMapper _mapper;
    private readonly IConfiguration _configuration;

    public ShortLinkRepository(ShortLinkContext context, IMapper mapper, IConfiguration configuration)
    {
        _context = context;
        _mapper = mapper;
        _configuration = configuration;
    }

    public async Task<object> GetAllShortLinksAsync(string userId)
    {
        var shortLinks = await _context.ShortLinks!
            .Where(l => l.CreatedBy == userId)
            .ToListAsync();
        var mappedLinks = _mapper.Map<List<ShortLink>>(shortLinks);
        var result = new List<object>();
        result.Add(new
        {
            result = "success",
            message = "Retrieved all short links successfully",
            total = mappedLinks.Count,
            data = mappedLinks
        });
        return result;
    }

    public async Task<ShortLink> GetShortLinkByIdAsync(int id)
    {
        var shortLink = await _context.ShortLinks!.FindAsync(id);
        return _mapper.Map<ShortLink>(shortLink);
    }

    public async Task<int> AddShortLinkAsync(ShortLink model)
    {
        var newShortLink = _mapper.Map<ShortLink>(model);
        var shotLinkDomain = _configuration.GetValue<string>("ShortLink:Domain");
        if (newShortLink.Password != null)
        {
            var passwordHashed = HashedPassword.HashPassword(newShortLink.Password);
            newShortLink.Password = passwordHashed;
        }

        if (newShortLink.AliasUrl == null)
        {
            var timer = new Timer(5 * 1000); // 5 seconds
            timer.Elapsed += (sender, e) => timer.Stop();
            timer.Start();

            string aliasUrl;
            do
            {
                aliasUrl = RandomString.GenerateRandomString(_configuration.GetValue<int>("ShortLink:LengthRandom"));
            } while (await AliasUrlExistsAsync(aliasUrl) && timer.Enabled);

            if (!timer.Enabled)
            {
                throw new Exception("time out");
            }

            newShortLink.AliasUrl = aliasUrl;
            newShortLink.FullAliasUrl = $"{shotLinkDomain}/{aliasUrl}";
            newShortLink.CreatedAt = DateTime.Now;
        }

        newShortLink.FullAliasUrl = $"{shotLinkDomain}/{newShortLink.AliasUrl}";

        newShortLink.Status = ShortLink.StatusActive;
        _context.ShortLinks!.Add(newShortLink);
        await _context.SaveChangesAsync();
        return newShortLink.Id;
    }

    public async Task<ShortLink> GetShortLinkByAliasUrlAsync(string aliasUrl)
    {
        if (string.IsNullOrEmpty(aliasUrl))
        {
            return null;
        }

        var shortLink = await _context.ShortLinks!.FirstOrDefaultAsync(x => x.AliasUrl == aliasUrl);
        return _mapper.Map<ShortLink>(shortLink);
    }

    public async Task UpdateShortLinkAsync(int id, ShortLink model)
    {
        if (id == model.Id)
        {
            var shortLink = _mapper.Map<ShortLink>(model);
            shortLink.UpdatedAt = DateTime.Now;
            _context.ShortLinks!.Update(shortLink);
            await _context.SaveChangesAsync();
        }
        else
        {
            throw new Exception($"ShortLink with aliasUrl: {id} does not exist");
        }
    }

    public async Task IncreaseTotalClickAsync(string aliasUrl)
    {
        var shortLink = await _context.ShortLinks!.FirstOrDefaultAsync(x => x.AliasUrl == aliasUrl);
        if (shortLink != null)
        {
            shortLink.TotalClick++;
            _context.ShortLinks!.Update(shortLink);
            await _context.SaveChangesAsync();
        }
        else
        {
            throw new Exception($"ShortLink with aliasUrl: {aliasUrl} does not exist");
        }
    }

    public async Task DeleteShortLinkAsync(int id, ShortLink model)
    {
        var shortLink = _context.ShortLinks!.SingleOrDefault(b => b.Id == id);
        if (shortLink != null)
        {
            _context.ShortLinks!.Remove(shortLink);
            await _context.SaveChangesAsync();
        }
        else
        {
            throw new Exception($"ShortLink with id: {id} does not exist");
        }
    }

    private async Task<bool> AliasUrlExistsAsync(string aliasUrl)
    {
        return await Task.FromResult(
            _context.ShortLinks!
                .AsEnumerable()
                .Any(s => string.Equals(s.AliasUrl, aliasUrl, StringComparison.Ordinal))
        );
    }
}
