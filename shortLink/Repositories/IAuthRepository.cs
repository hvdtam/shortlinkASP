using Microsoft.AspNetCore.Identity;
using shortLink.Models.Auth;

namespace shortLink.Repositories;

public interface IAuthRepository
{
    public Task<IdentityResult> SignUpAsync(SignUpModel model);
    public Task<string> SignInAsync(SignInModel model);
}
