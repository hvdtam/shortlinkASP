using shortLink.Data;

namespace shortLink.Repositories;

public interface IShortLinkRepository
{
    public Task<object> GetAllShortLinksAsync(string userId);
    public Task<ShortLink> GetShortLinkByIdAsync(int id);
    public Task<int> AddShortLinkAsync(ShortLink shortLink);
    public Task<ShortLink> GetShortLinkByAliasUrlAsync(string aliasUrl);
    public Task UpdateShortLinkAsync(int id, ShortLink model);
    public Task DeleteShortLinkAsync(int id, ShortLink model);
    public Task IncreaseTotalClickAsync(string aliasUrl);
}
