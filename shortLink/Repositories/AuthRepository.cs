using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using shortLink.Data;
using shortLink.Models.Auth;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace shortLink.Repositories;

public class AuthRepository : IAuthRepository
{
    private readonly IConfiguration _configuration;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly UserManager<ApplicationUser> _userManager;

    public AuthRepository(UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager,
        IConfiguration configuration)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _configuration = configuration;
    }

    public async Task<IdentityResult> SignUpAsync(SignUpModel model)
    {
        var user = new ApplicationUser
        {
            UserName = model.Email,
            Email = model.Email
        };
        return await _userManager.CreateAsync(user, model.Password);
    }

    // public async Task<string> SignInAsync(SignInModel model)
    // {
    //     var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
    //     if (!result.Succeeded) return string.Empty;
    //
    //     var authenticationClaims = new List<Claim>
    //     {
    //         new(ClaimTypes.Email, model.Email),
    //         new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
    //     };
    //     var authSigningKey =
    //         new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("JWT:Secret")));
    //     var token = new JwtSecurityToken(
    //         _configuration.GetValue<string>("JWT:Secret"),
    //         _configuration.GetValue<string>("JWT:audience"),
    //         expires: DateTime.Now.AddHours(24),
    //         claims: authenticationClaims,
    //         signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha512)
    //     );
    //     return new JwtSecurityTokenHandler().WriteToken(token);
    // }
    public async Task<string> SignInAsync(SignInModel model)
    {
        var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
        if (!result.Succeeded)
        {
            return string.Empty;
        }

        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_configuration["JWT:Secret"]);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Email, model.Email)
            }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}
