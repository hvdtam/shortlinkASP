using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace shortLink.Data;

[Table("ShortLinks")]
public class ShortLink
{
    public const int StatusActive = 10;
    public const int StatusInActive = 0;

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required]
    [StringLength(1000, MinimumLength = 5)]
    public string? LongUrl { get; set; }

    // [Required]

    [StringLength(100, MinimumLength = 3)]
    [Index("Idx_AliasUrl", IsUnique = true)]
    public string? AliasUrl { get; set; }

    // [Required]
    [StringLength(200, MinimumLength = 5)]
    [Index("Idx_FullAliasUrl", IsUnique = true)]
    public string? FullAliasUrl { get; set; }

    public string? Password { get; set; }

    [DefaultValue(10)] public int Status { get; set; }

    public DateTimeOffset? Expire { get; set; }

    [DefaultValue(0)] public int? TotalClick { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    [StringLength(450)] [JsonIgnore] public string? CreatedBy { get; set; }
    [JsonIgnore] public DateTimeOffset UpdatedAt { get; set; }

    [StringLength(450)] [JsonIgnore] public string? UpdatedBy { get; set; }

    public bool IsExpired()
    {
        return Expire != null && Expire < DateTimeOffset.Now;
    }

    public bool IsPasswordProtected()
    {
        return !string.IsNullOrEmpty(Password);
    }

    public bool IsActive()
    {
        return Status == StatusActive;
    }

    public bool IsInActive()
    {
        return Status == StatusInActive;
    }
}
