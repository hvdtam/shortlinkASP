using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Newtonsoft.Json;

namespace shortLink.Data;

public class ShortLinkContext: IdentityDbContext<ApplicationUser>
{
    public ShortLinkContext(DbContextOptions<ShortLinkContext> options) : base(options)
    {

    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<ShortLink>(entity =>
        {
            entity.HasIndex(p => new { p.AliasUrl})
                .IsUnique();
            entity.HasIndex(p => new { p.FullAliasUrl})
                .IsUnique();
        });
        modelBuilder.Entity<ShortLink>()
            .Property(s => s.AliasUrl)
            .UseCollation("SQL_Latin1_General_CP1_CS_AS");

        modelBuilder.Entity<IdentityUserLogin<string>>()
            .HasKey(l => new { l.LoginProvider, l.ProviderKey });
    }

    #region DbSet

    public DbSet<ShortLink>? ShortLinks { get; set; }

    #endregion
}
